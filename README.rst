Robot Framework Workshop
========================

This is a test automation with test cases used to search and book for any flight.
The application under test is the demo version of `Simple Travel Agency <http://blazedemo.com/>`_


Introduction
------------

`Robot Framework <http://robotframework.org>`_ is a generic open source
automation framework for acceptance testing, acceptance test driven
development (ATDD), and robotic process automation (RPA). It has simple plain
text syntax and it can be extended easily with libraries implemented using
Python or Java.

References
----------

- `Robot FrameWork: The Ultimate Guide to Running Your Tests
  <https://www.blazemeter.com/blog/robot-framework-the-ultimate-guide-to-running-your-tests>`_
