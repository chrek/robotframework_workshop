*** Settings ***
Library     SeleniumLibrary

*** Variables ***
${URL}        https://blazedemo.com/
${BROWSER}    chrome

*** Keywords ***
Open blazedemo Home Page
    open browser  ${URL}    ${BROWSER}

Close Browsers
    close all browsers

Select Start City
    [Arguments]  ${start_city}
    select from list by value  xpath://select[@name='fromPort']  ${start_city}

Select End City
    [Arguments]  ${end_city}
    select from list by value  xpath://select[@name='toPort']  ${end_city}

Search for Flights
    click button  css:input[type='submit']

Flights are available
    @{flights}=  Get WebElements    css:table[class='table']>tbody tr
    Should Not Be Empty     ${flights}