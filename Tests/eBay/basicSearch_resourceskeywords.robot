*** Settings ***
Documentation     Basic Search Functionality using SeleniumLibrary.
Library           SeleniumLibrary
Resource          ../../Resources/CommonFunctionality.robot
Resource          ../../Resources/eBay_UserDefinedKeywords.robot

*** Variables ***


*** Test Cases ***
Verify Basic Search Functionality for eBay
    [documentation]  This test case will verify a basic search
    [tags]  functional

    Start TestCase
    Verify Search Results
    Filter Results by condition
    Verify Filter Results
    Finish TestCase

