*** Settings ***
Documentation     Basic Search Functionality using SeleniumLibrary.
Library           SeleniumLibrary

*** Variables ***


*** Keywords ***

*** Test Cases ***
Verify Basic Search Functionality for eBay
    [documentation]  This test case will verify a basic search
    [tags]  functional

    open browser  https://www.ebay.com  chrome
#    [Arguments]    ${username}
#        Input Text    username_field    ${username}
    maximize browser window
    input text  //*[@id="gh-ac"]   mobile
    press keys  //*[@id="gh-btn"]  [Return]
#    press keys  css://gh-btn  [Return]
    page should contain  results for mobile

    close browser
