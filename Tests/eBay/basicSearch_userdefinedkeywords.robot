*** Settings ***
Documentation     Basic Search Functionality using SeleniumLibrary.
Library           SeleniumLibrary

*** Variables ***


*** Keywords ***
Start TestCase
    open browser  https://www.ebay.com  chrome
    maximize browser window

Verify Search Results
    input text  //*[@id="gh-ac"]   mobile
    press keys  //*[@id="gh-btn"]  [Return]
    #    press keys  css://gh-btn  [Return]
    page should contain  results for mobile

Finish TestCase
    close browser

*** Test Cases ***
Verify Basic Search Functionality for eBay
    [documentation]  This test case will verify a basic search
    [tags]  functional

    Start TestCase
    Verify Search Results
    Finish TestCase
