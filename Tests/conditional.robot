*** Settings ***
Documentation  Conditional Testing On Element Visibility in Wikipedia
Library  SeleniumLibrary

*** Variables ***

*** Test Cases ***
Verify the presence of Wikivoyage, click and validate (IF condition )
    [documentation]    This test case verifies that if Wikivoyage is available
...                    on the page, user is able to click on it
    [tags]             Regression
    Open Browser       https://www.wikipedia.org/    ff
    ${count}=          get element count  xpath://span[contains(text(),'Wikivoyage')]
    log                ${count}         # 1 if element is present
    run keyword if  ${count} > 0    Click Wikivoyage
...        ELSE    Click Wiktionary
    title should be  Wikivoyage    timeout=5
    close browser

Verify that if Wikivoyage is not present, click on Wiktionary (ELSE condition)
    [documentation]    This test case verifies that if Wikivoyage is not available
...                    on the page, user should click Wiktionary
    [tags]             Regression
    Open Browser       https://www.wikipedia.org/    ff
    ${count}=          get element count  xpath://span[contains(text(),'NOWikivoyage')]
    log                ${count}         # = 0 for wrong locator (to favour ELSE condition)
    run keyword if  ${count} > 0    Click Wikivoyage
...        ELSE    Click Wiktionary
    title should be  Wiktionary    timeout=5
    close browser

*** Keywords ***
Click Wikivoyage
    click element  css:[data-jsl10n="wikivoyage.name"]
    #click element  xpath://span[contains(text(),'Wikivoyage')]

Click Wiktionary
    click element  css:[data-jsl10n="wiktionary.name"]
    #click element  xpath://span[contains(text(),'Wiktionary')]
