*** Settings ***
Library    SeleniumLibrary
Library    ExcelLibrary

*** Variables ***
${valid_username}           email-support@ngendigital.com
${invalid_username}         email*support@ngendigital.com
${valid_password}           abc123
${invalid_password}         abc-123

*** Keywords ***
Default Value Username Password
    Element Attribute Value Should Be    xpath://input[@type='email']      value   email
    Element Attribute Value Should Be    xpath://input[@type='password']   value   password


Login And Navigate To URL
    Open Browser    https://ngendigital.com/demo-application    ff
    Element Text Should Be    xpath://span[contains(text(), "Login to Flight Application")]    Login to Flight Application

Change To iFrame
    Select Frame    id=iframe-05

Object On Web Page
    Element Text Should Be         xpath://h1[contains(text(),'My Account')]     My Account
    Page Should Contain Element    xpath://input[@type='email']
    Page Should Contain Element    xpath://input[@type='password']
    Page Should Contain Element    xpath://div[@class='btn']
    Page Should Contain Element    xpath://*[@id="btn2"]
    Page Should Contain Element    xpath://p[contains(text(),'Forgot your password?')]
    Page Should Contain Element    xpath://u[@style='color:#f1c40f;']

Login Attempt Without Username
    Empty Login Fields
    Wait Until Keyword Succeeds    2    5   Input Password        xpath://input[@type='password']   ${valid_password}
    Wait Until Keyword Succeeds    2    5   Click Element         xpath://div[@class='btn']
    ${text1}=   Get Text    xpath://div[@id='MissingUname']//p//label//font[@color='red'][contains(text(),'Please enter username')]
    Run Keyword And Return If    "${text1}"=="Please enter username"    Log    ${text1}

Login Attempt Without Password
    Empty Login Fields
    Wait Until Keyword Succeeds    2    5   Input Text        xpath://input[@type='email']   ${valid_username}
    Wait Until Keyword Succeeds    2    5   Click Element         xpath://div[@class='btn']
    ${text1}=   Get Text    xpath://font[@color='red'][contains(text(),'Please enter password')]
    Run Keyword And Return If    "${text1}"=="Please enter password"    Log    ${text1}

Login Attempt Without Email and Password
    Empty Login Fields
    Wait Until Keyword Succeeds    2    5   Click Element         xpath://div[@class='btn']
    ${text1}=   Get Text    xpath://div[@id='MissingUnamePasswd']//p//label//font[@color='red'][contains(text(),'Please enter username and password')]
    Run Keyword If    "${text1}"=="Please enter username and password"    Log    ${text1}

Login Attempt With Invalid Email
    Empty Login Fields
    Wait Until Keyword Succeeds    2    5   Input Text        xpath://input[@type='email']          ${invalid_username}
    Wait Until Keyword Succeeds    2    5   Input Password        xpath://input[@type='password']   ${valid_password}
    Wait Until Keyword Succeeds    2    5   Click Element         xpath://div[@class='btn']
    ${text1}=   Get Text    xpath://font[@color='red'][contains(text(),'Invalid username/password')]
    Run Keyword And Return If    "${text1}"=="Invalid username/password"    Log    ${text1}

Login Attempt With Invalid Password
    Empty Login Fields
    Wait Until Keyword Succeeds    2    5   Input Text        xpath://input[@type='email']          ${valid_username}
    Wait Until Keyword Succeeds    2    5   Input Password        xpath://input[@type='password']   ${invalid_password}
    Wait Until Keyword Succeeds    2    5   Click Element         xpath://div[@class='btn']
    ${text1}=   Get Text    xpath://font[@color='red'][contains(text(),'Invalid username/password')]
    Run Keyword And Return If    "${text1}"=="Invalid username/password"    Log    ${text1}

Empty Login Fields
    Wait Until Keyword Succeeds    2    5   Clear Element Text    xpath://input[@type='email']
    Wait Until Keyword Succeeds    2    5   Clear Element Text    xpath://input[@type='password']

Get Data From Excel With The Given Column
    [Arguments]    ${sSheetname}    ${sTestcaseNo}   ${sColumnname}
    Log            ${sColumnname}
    ${columnCount}=     Get Column Count    ${sSheetname}
    Log            ${columnCount}
    :FOR    ${y}    IN RANGE    0   ${columnCount}
    ${Header}=      Read Cell Data By Coordinates