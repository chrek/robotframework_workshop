*** Settings ***
Library    SeleniumLibrary
Resource   Resources/commonkeywords.robot


*** Test Cases ***
TC01-Login screen-Objects on the page
    Login And Navigate To URL
    Change To IFrame
    Object On Web Page

    Close Browser

TC02_Login screen-Default text for Username and Password
    Login And Navigate To URL
    Change To IFrame
    Default Value Username Password

    Close Browser

TC03_User Login Attempt Without Email
    Set Selenium Speed    0.5
    Login And Navigate To URL
    Change To IFrame
    Empty Login Fields
    Login Attempt Without Username

    Close Browser

TC04_User Login Attempt Without Password
    Set Selenium Speed    0.5
    Login And Navigate To URL
    Change To IFrame
    Empty Login Fields
    Login Attempt Without Password

    Close Browser

TC05_User Login Attempt Without Email and Password
    Set Selenium Speed    0.5
    Login And Navigate To URL
    Change To IFrame
    Empty Login Fields
    Login Attempt Without Email And Password

    Close Browser

TC06_User Login Attempt With Invalid Email
    Set Selenium Speed    0.5
    Login And Navigate To URL
    Change To IFrame
    Empty Login Fields
    Login Attempt With Invalid Email

    Close Browser

TC07_User Login Attempt With Invalid Password
    Set Selenium Speed    0.5
    Login And Navigate To URL
    Change To IFrame
    Empty Login Fields
    Login Attempt With Invalid Password

    Close Browser

