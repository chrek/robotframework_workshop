*** Settings ***
Documentation       Calculate the sum of 2 numbers.
Library             function.py

*** Variables ***
# &{NUMBERS_TO_ADD}=    num1=502    num2=1000
${NUM1}     ${502}
${NUM2}=    ${205}


*** Test Cases ***
Add Two Numbers
    [Documentation]    This test case is for addition of two numbers
    # ${Sum}=     Print Sum    ${NUMBERS_TO_ADD}[num1]    ${NUMBERS_TO_ADD}[num2]
    ${Sum}=     Print Sum    ${NUM1}    ${NUM2}
