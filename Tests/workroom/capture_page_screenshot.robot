*** Settings ***
Documentation       Using A Robot Framework Library.
Library             SeleniumLibrary


*** Tasks ***
Take a screenshot of the Robocorp Front Page
    Open Browser    https://www.robocorp.com
    Sleep    1.5
    Capture Page Screenshot
