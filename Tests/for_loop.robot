*** Settings ***
Documentation  Control the test execution flow using the For Loop
...            in Robot Framework
Library    SeleniumLibrary
Library    BuiltIn
Library    Collections

*** Variables ***
@{OrangeRHMQuickLaunchList}  Assign Leave  Leave List  Timesheets  Apply Leave  My Leave  My Timesheet


*** Test Cases ***
Verify that the texts in the OrangeRHM Quick Launch Section are the same as the items in the OrangeRHMQuickLaunchList variable
    [documentation]    This test case verifies a match betwee OrangeRHMQuickLaunchList and
...                    and the texts on the Quick Launch Section of OrangeHRM
    [tags]  Smoke

    Login To OrangeRHM
    Element Should Be Visible    id:welcome    timeout=5

    @{elementList}=  Get WebElements    css:div.quickLaunge span
    @{textList}=     Create List
    FOR    ${element}    IN    @{elementList}
        ${text}=    Get Text    ${element}
        Append To List    ${textList}    ${text}
    END

    Log To Console    \n List from OrangeHRM WebPage:
    Log To Console    ${textList}
    Log To Console    Expected List stored in Variables section:
    Log To Console    ${OrangeRHMQuickLaunchList}
    Lists Should Be Equal    ${textList}  ${OrangeRHMQuickLaunchList}  timeout=5

    Close Browser


*** Keywords ***
Login To OrangeRHM
    Open Browser    https://opensource-demo.orangehrmlive.com/    ff
    Wait Until Element Is Visible    id:txtUsername    timeout=5
    Input Text    id:txtUsername     Admin
    Input Password    id:txtPassword    admin123
    Click Element    id:btnLogin
