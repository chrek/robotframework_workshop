*** Settings ***
Documentation  Radio Buttons in Robot Framework
Library        SeleniumLibrary

*** Variables ***

*** Test Cases ***
Verify the use of Radio Buttons in item selection
    [documentation]    This test case verifies that user can select a Radio button at a time.
    [tags]             Smoke

    Open Browser    https://demoqa.com/automation-practice-form     ff
    Maximize Browser Window
    Wait Until Element Is Visible   css:#genterWrapper     timeout=5
    Page Should Contain Element     css:#genterWrapper

    # NO Radio button is selected by default
    Radio Button Should Not Be Selected    gender

    #Sleep    10

    #Wait Until Element Is Visible    xpath://div[@class='Advertisement-Section']

    # Select Radio button for Male
    # You may click on the Label
    #Click Element    xpath://label[contains(text(),'Male')]
    Select Radio Button Via Label    xpath://label[contains(text(),'Male')]
    #Select Radio Button             gender   Male
    Radio Button Should Be Set To    gender   Male

    # Select Radio button for Female
    # You may click on the Label
    #Select Radio Button             gender   Female
    Select Radio Button Via Label    xpath://label[contains(text(),'Female')]
    Radio Button Should Be Set To    gender   Female

    # Select Radio button for Other
    # Select Radio Button            gender   Other
    Select Radio Button Via Label    xpath://label[contains(text(),'Other')]
    Radio Button Should Be Set To    gender   Other

    Close Browser

*** Keywords ***
Select Radio Button Via Label
    [documentation]    Select Radio button by clicking its label
    [Arguments]    ${argument}
    Click Element   ${argument}