*** Settings ***
Library  SeleniumLibrary
Documentation    Test a Python Script (AUT: Employee.py)
Library          OperatingSystem


*** Variables ***
${AUT}      python employee.py

*** Test Cases ***
Empty Employees List
    [Documentation]     Verify that Employees list is Empty
    [Setup]             Clear Employees List
    # ${rc}               ${output}=      Run And Return Rc And Output    ${AUT} list_employees
    # Should Be Equal As Integers     ${rc}           0
    ${output}=      Get Employees List
    Should Be Equal                 ${output}       []


Add Employee
    [Documentation]     Verify that User Can add Employees
    [Setup]             Clear Employees List
    Add Employee    Mary    Joe
    ${output}=      Get Employees List
    Should Be Equal                 ${output}       ['Mary Joe']
    [Teardown]      Clear Employees List



*** Keywords ***
Clear Employees List
    [Documentation]    Remove All Entries in the Employees List
    ${rc}               ${output}=      Run And Return Rc And Output    ${AUT} remove_all_employees
    Should Be Equal As Integers     ${rc}       0


Get Employees List
    [Documentation]    Get the Employees List
    ${rc}               ${output}=      Run And Return Rc And Output    ${AUT} list_employees
    Should Be Equal As Integers     ${rc}       0
    [Return]    ${output}


Add Employee
    [Documentation]     Add an Employee
    [Arguments]         ${first_name}   ${last_name}
    ${rc}               ${output}=      Run And Return Rc And Output    ${AUT} add_employee ${first_name} ${last_name}
    Should Be Equal As Integers     ${rc}           0

