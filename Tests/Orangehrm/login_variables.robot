*** Settings ***
Documentation  Login Functionality
Library  SeleniumLibrary

*** Variables ***
${HRMUserName}    Admin
${HRMPassword}    admin123
@{HerokuAppCredentials}    tomsmith    SuperSecretPassword!
&{VisibleElements}    OrangeHRM=id:welcome    InternetHerokuApp=css:[href="/logout"]

*** Test Cases ***
Verify Successful Login to OrangeHRM
    [documentation]    This test case verifies that user is able to successfully Login to OrangeHRM
    [tags]  Smoke
    Open Browser    https://opensource-demo.orangehrmlive.com/    ff
    Wait Until Element Is Visible    id:txtUsername    timeout=5
    Input Text    id:txtUsername     ${HRMUserName}
    Input Password    id:txtPassword    ${HRMPassword}
    Click Element    id:btnLogin
    Element Should Be Visible    ${VisibleElements}[OrangeHRM]    timeout=5
    Close Browser

Verify Successful Login to the-internet.herokuapp
    [documentation]  This test case verifies that user is able to successfully Login to the-internet.herokuapp
    [tags]  Smoke
    Open Browser    https://the-internet.herokuapp.com/login    ff
    Wait Until Element Is Visible    id:username    timeout=5
    Input Text    id:username    ${HerokuAppCredentials}[0]
    Input Password    id:password    ${HerokuAppCredentials}[1]
    Click Element    css:button[type="submit"]
    Element Should Be Visible    ${VisibleElements}[InternetHerokuApp]    timeout=5
    Close Browser

*** Keywords ***