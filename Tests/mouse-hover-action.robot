*** Settings ***
Documentation  Mouse Hover Actions in Robot Framework
Library        SeleniumLibrary

*** Variables ***
${income}

*** Test Cases ***
Verify the use of Mouse Hover Action on HTML Elements
    [documentation]    This test case verifies that user can hover on elements and confirm effects.
    [tags]             Regression

    Open Browser    https://the-internet.herokuapp.com/hovers     ff
    Wait Until Element Is Visible    id:content     timeout=5

    # Mouse over the first image
    Mouse Over    xpath://div[@class='example']/div[1]/img
    Element Text Should Be    xpath://h5[contains(text(),'name: user1')]     name: user1     timeout=5

    # Mouse over th second image
    Mouse Over    xpath://div[@class='example']/div[2]/img
    Element Text Should Be    xpath://h5[contains(text(),'name: user2')]     name: user2     timeout=5

    # Mouse over the third image
    Mouse Over    xpath://div[@class='example']/div[3]/img
    Element Text Should Be    xpath://h5[contains(text(),'name: user3')]     name: user3     timeout=5

    Close Browser
