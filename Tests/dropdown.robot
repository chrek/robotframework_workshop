*** Settings ***
Documentation  Dropdown Buttons in Robot Framework
Library        SeleniumLibrary

*** Variables ***

*** Test Cases ***
Verify the use of Dropdown button in item selection
    [documentation]    This test case verifies that user can select items from dropdown button.
    [tags]             Smoke

    Open Browser    https://the-internet.herokuapp.com/dropdown     ff
    Wait Until Element Is Visible    id:dropdown     timeout=5
    Select From List By Index        id:dropdown     1
    List Selection Should Be         id:dropdown     Option 1

    Select From List By Index        id:dropdown     2
    List Selection Should Be         id:dropdown     Option 2

    Close Browser
