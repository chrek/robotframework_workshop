*** Settings ***
Documentation  Drag and Drop Actions in Robot Framework
Library        SeleniumLibrary

*** Variables ***

*** Test Cases ***
Verify the use of Drag and Drop of HTML Elements are possible
    [documentation]    This test case verifies that user can drag and drop HTML elements within the page.
    [tags]             Regression

    Open Browser    https://demoqa.com/droppable     ff
    Maximize Browser Window
    Wait Until Element Is Visible    id:draggable     timeout=5
    Wait Until Element Is Visible    id:droppable     timeout=5
    # Verify that the Text 'Drop here' is present before Drag and Drop
    Element Text Should Be    id:droppable    Drop here    timeout=5
    Drag And Drop    xpath://div[@id='draggable']    xpath://div[@id='droppable']
    # Verify that the Text now (after Drag and Drop) is 'Dropped!'
    Element Text Should Be    id:droppable    Dropped!    timeout=5

    Close Browser
