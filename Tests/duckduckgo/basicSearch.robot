*** Settings ***
Documentation       Basic Search Functionality using SeleniumLibrary.
Library             SeleniumLibrary

*** Variables ***
${RF_URL}           https://robotframework.org/
${DUCK_URL}         https://duckduckgo.com/
${RF_RESULT_XPATH}         //article[@id='r1-0']//div[@class='ikg2IXiCD14iVX7AdZo1']//h2[@class='LnpumSThxEWMIsDdAT17 CXMyPcQ6nDv47DKFeywM']//a[@class='eVNpHGjtxRBq_gLOfGDr']

*** Keywords ***
Open Duckduckgo Site
    [Documentation]     Open Duckduckgo Search functionalty
    Open Browser        ${DUCK_URL}     ff

Search For Robot Framework
    Input Query
    Click Search


Input Query
    Input Text    xpath://input[@id='search_form_input_homepage']       Robot Framework

Click Search
    Click Element    css:#search_button_homepage

Assert First Search Result is RF Homepage
    # xpath://article[@id='r1-0']
	Wait Until Page Contains Element 	id:r1-0
	${URL}= 	Get Element Attribute 	${RF_RESULT_XPATH}      href
	Should Be Equal 	${URL}      ${RF_URL}

*** Test Cases ***
Verify DuckDuckGo Basic Search Functionality for RF
    [Documentation]    This test case will verify a basic search
    [Tags]    functional

    [Setup]    Open Duckduckgo Site
    Maximize Browser Window
    Search For Robot Framework
    Assert First Search Result is RF Homepage
    [Teardown]    Close All Browsers
