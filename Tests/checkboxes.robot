*** Settings ***
Documentation  Checkboxes in Robot Framework
Library        SeleniumLibrary

*** Variables ***

*** Test Cases ***
Verify the use of Checkboxes in item selection
    [documentation]    This test case verifies that user can select items from checkboxes.
    [tags]             Smoke

    Open Browser    https://the-internet.herokuapp.com/checkboxes     ff
    Wait Until Element Is Visible    css:#checkboxes     timeout=5
    Page Should Contain Checkbox    xpath://form[@id='checkboxes']/input[1]
    # Checkbox 1 is not selected by default
    Checkbox Should Not Be Selected    xpath://form[@id='checkboxes']/input[1]

    # Checkbox 2 is selected by default
    Checkbox Should Be Selected    xpath://form[@id='checkboxes']/input[2]

    # Select Checkbox 1
    Select Checkbox                 xpath://form[@id='checkboxes']/input[1]
    Checkbox Should Be Selected     xpath://form[@id='checkboxes']/input[1]

    # Unselect Checkbox 2
    Unselect Checkbox                  xpath://form[@id='checkboxes']/input[2]
    Checkbox Should Not Be Selected        xpath://form[@id='checkboxes']/input[2]

    Close Browser
