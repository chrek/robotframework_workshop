*** Settings ***
Documentation  Navigating between Multiple Browser Windows in Robot Framework
Library        SeleniumLibrary

*** Variables ***

*** Test Cases ***
Switch between browser windows and verify Browser Title
    [documentation]    This test case verifies that user can switch between browsers.
    [tags]             Smoke

    Open Browser    https://the-internet.herokuapp.com/windows     ff
    Wait Until Element Is Visible    css:.example > h3     timeout=5
    Title Should Be    The Internet
    Click Element    xpath://a[contains(text(),'Click Here')]
    # We now have 2 Windows
    # Use the window title to switch to the original window
    Switch Window    title:The Internet
    # Verify the current window has the text 'Opening a new window'
    Element Text Should Be    css:.example > h3    Opening a new window     timeout=5

    # Use the window title to switch to the other window
    Switch Window    title:New Window
    # Verify the current window has the text 'New Window'
    Element Text Should Be    xpath://h3[contains(text(),'New Window')]    New Window      timeout=5     timeout=5

    Close Browser

Switch between browser windows using "Get Window Handle" and verify text on the Page
    [documentation]    This test case verifies that user can switch between browsers.
    [tags]             Smoke

    Open Browser    https://the-internet.herokuapp.com/windows     ff
    Wait Until Element Is Visible    css:.example > h3     timeout=5
    Title Should Be    The Internet
    Click Element    xpath://a[contains(text(),'Click Here')]
    # We now have 2 Windows
    # Use the window handle to switch to the original window
    ${handles}=      Get Window Handles
    Switch Window    ${handles}[0]
    # Verify the current window has the text 'Opening a new window'
    Element Text Should Be    css:.example > h3    Opening a new window     timeout=5

    # Use the window handle to switch to the other window
    Switch Window    ${handles}[1]
    # Verify the current window has the text 'New Window'
    Element Text Should Be    xpath://h3[contains(text(),'New Window')]    New Window      timeout=5     timeout=5

    Close Browser
