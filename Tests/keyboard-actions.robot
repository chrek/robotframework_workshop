*** Settings ***
Documentation  Keyboard Actions in Robot Framework
Library        SeleniumLibrary

*** Variables ***

*** Test Cases ***
Verify the use of Space Key Press as Keyboard Action
    [documentation]    This test case verifies that user can use Space Key in Input box.
    [tags]             Regression

    Open Browser    https://the-internet.herokuapp.com/key_presses     ff
    Wait Until Element Is Visible    css:#target     timeout=5
    Press Keys    css:#target      SPACE

    # Verify the text displayed
    Element Text Should Be    css:#result    You entered: SPACE     timeout=5

    Close Browser
