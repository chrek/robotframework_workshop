*** Settings ***
Library  SeleniumLibrary
Resource        ../../Resources/flights_search_keywords.robot

*** Test Cases ***
The user can search for flights
    [Tags]              search_flights
    Select Start City   Portland
    Select End City     New York
    Search for Flights
    Flights are available
