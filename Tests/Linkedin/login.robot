*** Settings ***
Documentation  Login Functionality for Linkedin
Library  SeleniumLibrary

*** Variables ***
${EMAIL}            wrongemail@gmail.com
${PASSWORD}         wrongpassword
${BROWSER}          Chrome
${URL}              https://www.linkedin.com/login
#${LOGINPAGETITLE}   LinkedIn Login, Sign in | LinkedIn
${LOGINPAGETITLE}   S’identifier sur LinkedIn | LinkedIn
${DASHBOARDTITLE} 	    World’s Largest Professional Network | LinkedIn
${EXPECTEDWARNINGMESSAGE}   That’s not the right password.
${WARNINGMESSAGE} 	Login Failed!
${DELAY} 			5s

*** Test Cases ***
Wrong Email
    [documentation]    Login Should Fail With Wrong Email
    [tags]  Smoke
    Open LinkedinPage
    Check LOgin Page Title
    Enter Email Address
    Enter Wrong Password
    Click Login
    sleep   ${Delay}
    Assert Warning Message
    [Teardown]  Close Browser


*** Keywords ***
Open LinkedinPage
    Open Browser    ${URL}      ${BROWSER}
    Maximize Browser Window

Enter Email Address
    Input Text        id:username           ${EMAIL}

Enter Wrong Password
    Input Password    id:password      ${PASSWORD}

Click Login
    Click Button       xpath://button[@type='submit']

Check LOgin Page Title
    Title Should be 	${LOGINPAGETITLE}

Assert Warning Message
    Element Text Should Be  xpath://div[@id='error-for-password']   ${ExpectedWarningMessage}   ${WarningMessage}
