*** Settings ***
Documentation  Accessing Shadow DOM in Robot Framework
Library        SeleniumLibrary

*** Variables ***
${JSPath}=      document.querySelector('shop-app').shadowRoot
...             .querySelector('iron-pages')
...             .querySelector('shop-list.iron-selected').shadowRoot
...             .querySelector('ul.grid > li > a')
...             .querySelector('shop-list-item').shadowRoot
...             .querySelector('div.title')

*** Test Cases ***
Verify the title of an item in Dhadow DOM
    [documentation]    This test case verifies the title of an item in the Shadow DOM.
    [tags]             Smoke

    Open Browser    https://shop.polymer-project.org/list/mens_outerwear     ff
    Wait Until Page Contains Element    xpath://shop-app[@page='list']     timeout=5    #OK
    Element Text Should Be    dom:${JSPath}    Men\'s Tech Shell Full-Zip
    Close Browser
