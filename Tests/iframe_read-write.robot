*** Settings ***
Documentation  Writing to and Reading from Iframes in Robot Framework
Library        SeleniumLibrary

*** Variables ***

*** Test Cases ***
Verify that user can write to and reade from Iframes
    [documentation]    This test case verifies that user can read from and write to an Iframe.
    [tags]             Regression

    Open Browser    https://the-internet.herokuapp.com/iframe     ff
    #Wait Until Element Is Visible    css:[role="menubar"]     timeout=5    #OK
    Wait Until Element Is Visible    xpath://div[@class='tox-menubar']    timeout=5
    Select Frame              id:mce_0_ifr
    Click Element             id:tinymce
    Clear Element Text        id:tinymce
    Input Text                id:tinymce    Input from Robot Framework Test
    Element Text Should Be    id:tinymce    Input from Robot Framework Test

    Close Browser


Verify that user can write to and reade Nested Iframes
    [documentation]    This test case verifies that user can read from and write to a nested Iframe.
    [tags]             Regression

    Open Browser    https://the-internet.herokuapp.com/nested_frames     ff
    Wait Until Element Is Visible    css:[frameborder="1"]     timeout=5    #OK
    Select Frame                    css:[src="/frame_top"]
    Select Frame                    css:[src="/frame_left"]
    Current Frame Should Contain    LEFT
    Unselect Frame

    Select Frame                    css:[src="/frame_top"]
    Select Frame                    css:[src="/frame_middle"]
    Current Frame Should Contain    MIDDLE
    Unselect Frame

    Select Frame                    css:[src="/frame_top"]
    Select Frame                    css:[src="/frame_right"]
    Current Frame Should Contain    RIGHT
    Unselect Frame

    Select Frame                    css:[src="/frame_bottom"]
    Current Frame Should Contain    BOTTOM
    Unselect Frame

    Close Browser